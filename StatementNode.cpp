#include "StatementNode.h"

AssignationNode::AssignationNode(IdNode* leftValue, ExpresionNode* rightValue)
{
	this->LeftValue = leftValue;
	this->RightValue = rightValue;
}
void AssignationNode::ValidateSemantics()
{
	BaseType* leftType = LeftValue->ValidateSemantics();
	BaseType* rightType = RightValue->ValidateSemantics();
	if (!leftType->IsAssignable(rightType))
	{
		printf("No se puede Asignar ");
		exit(0);
	}

}
void AssignationNode::Interpret()
{
	ExpresionValue * rightValue = RightValue->Interpret();
	SymbolsTable::getInstance().SetVariableValue(LeftValue->Name, rightValue);
}
StatementsType AssignationNode::GetType()
{
	return AssignationType;
}

ReadNode::ReadNode(IdNodeList * id_node_list)
{
	this->id_node_list = id_node_list;
}
ReadNode::~ReadNode()
{
}
void ReadNode::ValidateSemantics()
{
	for (IdNodeList::iterator i = id_node_list->begin(); i != id_node_list->end(); ++i)
	{

		BaseType* bt = (*i)->ValidateSemantics();

		if(bt->getType() != NumberTypeClass || bt->getType() != BoolTypeClass){
			if(bt->getType() != ArrayTypeClass){
				printf("Tipo de Variable no soportado para lectura\n");
				exit(0);
			}	
		}

		//TODO: delete bt;
	}
}
void ReadNode::Interpret()
{
	//TODO: hacer la lectura
}

StatementsType ReadNode::GetType(){
	return ReadType;
}

ForNode::ForNode(ExpresionList* initialExp, ExpresionNode* condition, StatementList* incrementStatement, BlockNode* block)
{
	ExpresionStatementList *esl = new ExpresionStatementList();
	for (ExpresionList::iterator i = initialExp->begin(); i != initialExp->end(); ++i)
	{
		esl->push_back(new ExpresionStatementNode((*i)));
	}
	this->initialExp = esl;
	this->condition = condition;
	this->incrementStatement = incrementStatement;
	this->block = block;
}

ForNode::~ForNode()
{
}

void ForNode::ValidateSemantics()
{

	for (ExpresionStatementList::iterator i = initialExp->begin(); i != initialExp->end(); i++)
	{
		((ExpresionStatementNode*)*i)->ValidateSemantics();
	}

	condition->ValidateSemantics();

	for (StatementList::iterator i = incrementStatement->begin(); i!=incrementStatement->end(); i++)
	{
		((AssignationNode*)*i)->ValidateSemantics();
	}

	block->ValidateSemantics();

}

void ForNode::Interpret()
{
	for (ExpresionStatementList::iterator i = initialExp->begin(); i != initialExp->end(); i++)
	{
		((ExpresionStatementNode*)*i)->Interpret();
	}

	ExpresionValue* ev = condition->Interpret();
	int val = 0;
	if(ev->getType() == NumberValueClass)
		val = ((NumberValue*)ev)->Value;
	else if(ev->getType() == BoolValueClass)
		val = ((BoolValue*)ev)->Value;

	while (val)
	{
		block->Interpret();

		for (StatementList::iterator i = incrementStatement->begin(); i != incrementStatement->end(); i++)
		{
			((AssignationNode*)*i)->Interpret();
		}

		if(ev->getType() == NumberValueClass)
			val = ((NumberValue*)ev)->Value;
		else if(ev->getType() == BoolValueClass)
			val = ((BoolValue*)ev)->Value;
	}
	
}

StatementsType ForNode::GetType()
{
	return ForType;
}


PrintNode::PrintNode(ExpresionList* expresiont_list)
{
	this->expresiont_list = expresiont_list;
}

PrintNode::~PrintNode()
{
}
void PrintNode::ValidateSemantics()
{
	for (ExpresionList::iterator i = expresiont_list->begin(); i != expresiont_list->end(); ++i)
	{
		(*i)->ValidateSemantics();
	}

}
void PrintNode::Interpret()
{
	for (ExpresionList::iterator i = expresiont_list->begin(); i != expresiont_list->end(); ++i)
	{
		ExpresionValue *ev = (*i)->Interpret();

		switch(ev->getType()){
			case NumberValueClass:
				cout << ((NumberValue*)ev)->Value;
				break;
			case BoolValueClass:
				cout << ((BoolValue*)ev)->Value;
				break;
			case StringValueClass:
				cout << ((StringValue*)ev)->Value;
				break;
			case CharacterValueClass:
				cout << ((CharacterValue*)ev)->Value;
				break;
			case ArrayValueClass:
				//TODO: implementar la impresion de arreglo
				break;
		}

	}

}
StatementsType PrintNode::GetType()
{
	return PrintType;
}

IfNode::IfNode(ExpresionNode* condition, BlockNode* block, BlockNode* elseBlock)
{
	this->Condition = condition;
	this->Block = block;
	this->ElseBlock = block;
}

IfNode::~IfNode()
{
}

void IfNode::Interpret()
{
	int condicion = ((NumberValue*)Condition->Interpret())->Value;
	if (condicion)
	{
		Block->Interpret();
	}
	else
	{
		if(ElseBlock != NULL)
			ElseBlock->Interpret();
	}
}
void IfNode::ValidateSemantics()
{
	Condition->ValidateSemantics();

	Block->ValidateSemantics();

	if(ElseBlock != NULL)
		ElseBlock->ValidateSemantics();
}

StatementsType IfNode::GetType()
{
	return IfType;
}

WhileNode::WhileNode()
{
}
WhileNode::WhileNode(ExpresionNode* condition, BlockNode*  block)
{
	this->Condition = condition;
	this->Block = block;
}
WhileNode::~WhileNode()
{
}
void WhileNode::Interpret()
{
	int condicion = ((NumberValue*)Condition->Interpret())->Value;
	while(condicion)
	{
		Block->Interpret();
	}
}
void WhileNode::ValidateSemantics()
{
	Condition->ValidateSemantics();

	Block->ValidateSemantics();


}

StatementsType WhileNode::GetType()
{
	return WhileType;
}

ReturnNode::ReturnNode()
{
}
ReturnNode::ReturnNode(ExpresionNode* expresion)
{
	this->Expresion = expresion;
}
ReturnNode::~ReturnNode()
{
}
void ReturnNode::Interpret()
{
	Expresion->Interpret();
	
}
void ReturnNode::ValidateSemantics()
{
	if (Expresion != NULL){
		Expresion->ValidateSemantics();
	}
}

StatementsType ReturnNode::GetType()
{
	return ReadType;
}


BreakNode::BreakNode()
{
}

BreakNode::~BreakNode()
{
}
void BreakNode::Interpret()
{

}
void BreakNode::ValidateSemantics()
{
	
}

StatementsType BreakNode::GetType()
{
	return BreakType;
}

ContinueNode::ContinueNode()
{
}

ContinueNode::~ContinueNode()
{
}
void ContinueNode::Interpret()
{

}
void ContinueNode::ValidateSemantics()
{

}

StatementsType ContinueNode::GetType()
{
	return BreakType;
}

VarDeclarationNode::VarDeclarationNode(BaseType * type, list<string> *names)
{
	this->Type = type;
	this->names = names;
}

VarDeclarationNode::VarDeclarationNode(BaseType * type, string name, ExpresionNode * value)
{
	this->Type = type;
	this->Name = name;
	this->Value = value;
}
VarDeclarationNode::~VarDeclarationNode()
{

}
void VarDeclarationNode::Interpret()
{
	//No sed que voy a interpretar

}
void VarDeclarationNode::ValidateSemantics()
{
	if (this->Value != NULL){
		BaseType *valueType = Value->ValidateSemantics();
		if (valueType->getType() != Type->getType())
		{
			printf("No se puede asignar  tipos diferente");
			exit(0);
		}
	}
	
}

StatementsType VarDeclarationNode::GetType()
{
	return VarDeclarationType;
}

BlockNode::BlockNode(StatementList* statement_list)
{
	this->statement_list = statement_list;
}

BlockNode::~BlockNode()
{
}

void BlockNode::ValidateSemantics(){

}

void BlockNode::Interpret(){

}

StatementsType BlockNode::GetType(){
	return BlockNodeType;
}

ExpresionStatementNode::ExpresionStatementNode(ExpresionNode * expresion_node){
	this->expresion_node = expresion_node;
}

ExpresionStatementNode::~ExpresionStatementNode(){

}

void ExpresionStatementNode::ValidateSemantics(){

}

void ExpresionStatementNode::Interpret(){

}

StatementsType ExpresionStatementNode::GetType(){
	return ExpresionStatementNodeType;
}







