%{
	#include <iostream>
  #include <utility>
  #include "BaseType.h"
  #include "ExpresionValue.h"
  #include "ExpresionNode.h"
  #include "StatementNode.h"

	extern int line;

	int yylex();

	void yyerror(const char* str){
		printf("Line %d: %s\n", line, str);
	}

	#define YYERROR_VERBOSE 1

  ExpresionList * result = 0;
%}

%union{
	char * s_id;
	int i_dec;
	char * s_hex;
	char * s_string;
	char c_character;
  ExpresionNode* exp_node;
  ExpresionList* exp_lst;
  StatementNode* stmt_node;
  StatementList* stmt_lst;
  list<string>* id_names;
  BaseType * type;
  IdNodeList *inl;
}

%token<s_id> ID
%token<i_dec> INT
%token<s_hex> HEX
%token<s_string> STRING
%token<c_character> CHARACTER
%token KW_TRUE KW_FALSE KW_NULL
%token KW_VOID KW_BOOL KW_INT
%token KW_BREAK KW_CONTINUE
%token KW_IF KW_ELSE
%token KW_WHILE KW_FOR
%token KW_PRINT KW_READ KW_NEW KW_RETURN
%token KW_CLASS KW_EXTENDS
%token LCURLY RCURLY LSQUARE RSQUARE LPARENT RPARENT
%token COMMA SEMICOLON OP_DOT
%token OP_ASSIGN
%token OP_NEGATION
%token OP_MOD
%token OP_MINUS OP_PLUS OP_MULTIPLY OP_DIVISION
%token OP_RSHIFT OP_LSHIFT OP_ROT
%token OP_LESS OP_LESSEQUAL OP_GREATEREQUAL OP_GREATER
%token OP_EQUAL OP_NOTEQUAL
%token OP_AND OP_OR

%type<exp_node> bool_constant constant
%type<exp_node> expression left_value argument
%type<exp_node> method_call
%type<exp_lst> expression_opt expression_list argument_list 
%type<inl> left_value_list
%type<stmt_node> instructions var_declaration statement
%type<stmt_lst> instructions_opt instructions_list
%type<stmt_node> assign block else_block
%type<stmt_lst> assign_opt assign_list
%type<id_names> id_list
%type<type> var_type


%left OP_ASSIGN
%left OP_OR
%left OP_AND
%nonassoc OP_EQUAL OP_NOTEQUAL
%nonassoc OP_LESS OP_GREATER OP_LESSEQUAL OP_GREATEREQUAL
%left OP_LSHIFT OP_RSHIFT OP_ROT
%left OP_PLUS OP_MINUS
%left OP_MULTIPLY OP_DIVISION OP_MOD
%right OP_NEGATION UMINUS

%%
program: KW_CLASS ID LCURLY member_declaration_opt RCURLY
;

member_declaration_opt: member_declaration_list
                      |
;

member_declaration_list: member_declaration_list member_declaration
                       | member_declaration
;

member_declaration: field_declaration
                  | method_declaration
;

field_declaration: var_type ID OP_ASSIGN constant SEMICOLON
                 | var_type field_list SEMICOLON
;


var_type: KW_INT {$$ = new NumberType();}
        | KW_BOOL {$$ = new BoolType();}
;

constant: INT {$$ = new NumberNode(yylval.i_dec);}
        | HEX {$$ = new NumberNode(yylval.s_hex);}
        | CHARACTER {$$ = new CharacterNode(yylval.c_character);}
        | bool_constant {$$ = $1;}
;

bool_constant: KW_TRUE {$$ = new BoolNode(true);}
             | KW_FALSE {$$ = new BoolNode(false);}
;

field_list: field_list COMMA field
          | field
;

field: ID
     | ID LSQUARE INT RSQUARE
;

method_declaration: var_type ID LPARENT parameters_opt RPARENT block
                  | KW_VOID ID LPARENT parameters_opt RPARENT block
;

parameters_opt: parameters_list
              |
;

parameters_list: parameters_list COMMA parameter
               | parameter
;

parameter: var_type ID
;

block: LCURLY instructions_opt RCURLY {$$ = new BlockNode($2);}
;

instructions_opt: instructions_list {$$ = $1;}
                | {$$ = NULL;}
;

instructions_list: instructions_list instructions {$1->push_back($2); $$ = $1;}
                 | instructions {StatementList* st = new StatementList(); st->push_back($1); $$ = st;}
;

instructions: var_declaration {$$ = $1;}
            | statement {$$ = $1;}
;

var_declaration: var_type ID OP_ASSIGN constant SEMICOLON {$$ = new VarDeclarationNode($1, $2, $4); }
               | var_type id_list SEMICOLON {$$ = new VarDeclarationNode($1, $2);}
;

id_list: id_list COMMA ID {$1->push_back($3); $$ = $1;}
       | ID {list<string> *names = new list<string>(); names->push_back($1); $$=names;}
;

statement: assign SEMICOLON {$$ = $1;}
         | method_call SEMICOLON {$$ = new ExpresionStatementNode($1);}
         | KW_IF LPARENT expression RPARENT block else_block {$$ = new IfNode($3, (BlockNode*)$5, (BlockNode*)$6);}
         | KW_WHILE LPARENT expression RPARENT block {$$ = new WhileNode($3, (BlockNode*)$5);}
         | KW_FOR LPARENT expression_opt SEMICOLON expression SEMICOLON assign_opt RPARENT block {$$ = new ForNode($3, $5, $7, (BlockNode*)$9);}
         | KW_RETURN expression SEMICOLON {$$ = new ReturnNode($2);}
         | KW_RETURN SEMICOLON {$$ = new ReturnNode(); }
         | KW_BREAK SEMICOLON {$$ = new BreakNode();}
         | KW_CONTINUE SEMICOLON {$$ = new ContinueNode();}
         | KW_PRINT argument_list {$$ = new PrintNode($2);}
         | KW_READ left_value_list {$$ = new ReadNode($2);}
         | block {$$ = $1;}
;


assign_opt: assign_list {$$ = $1;}
           | {$$ = NULL;}
;

assign_list: assign_list COMMA assign {$1->push_back($3); $$ = $1;}
           | assign {StatementList *st = new StatementList(); st->push_back($1); $$ = st;}
;

assign: left_value OP_ASSIGN expression {$$ = new AssignationNode((IdNode*)$1, $3);}
;


left_value: ID {$$ = new IdNode($1, NULL);}
          | ID LSQUARE expression RSQUARE {$$ = new IdNode($1,new ArrayAccesor($3));}
;

expression: constant {$$ = $1;}
          | left_value {$$ = $1;}
          | method_call {$$ = $1;}
          | expression OP_OR expression {$$ = new OrOperatorNode($1, $3);}
          | expression OP_AND expression {$$ = new AndOperatorNode($1, $3);}
          | expression OP_NOTEQUAL expression {$$ = new NotEqualsOperatorNode($1, $3);}
          | expression OP_EQUAL expression {$$ = new EqualsOperatorNode($1, $3);}
          | expression OP_LESS expression {$$ = new LessThanOperatorNode($1, $3);}
          | expression OP_GREATER expression {$$ = new GreaterThanOperatorNode($1, $3);}
          | expression OP_LESSEQUAL expression {$$ = new LessEqualsThanOperatorNode($1, $3);}
          | expression OP_GREATEREQUAL expression {$$ = new GreaterEqualsThanOperatorNode($1, $3);}
          | expression OP_LSHIFT expression {$$ = new LeftShiftOperatorNode($1, $3);}
          | expression OP_RSHIFT expression {$$ = new RightShiftOperatorNode($1, $3);}
          | expression OP_ROT expression {$$ = new RotOperatorNode($1, $3);}
          | expression OP_PLUS expression {$$ = new SumOperatorNode($1, $3);}
          | expression OP_MINUS expression {$$ = new SubstractOperatorNode($1, $3);}
          | expression OP_MULTIPLY expression {$$ = new MultiplyOperatorNode($1, $3);}
          | expression OP_DIVISION expression {$$ = new DivisionOperatorNode($1, $3);}
          | expression OP_MOD expression {$$ = new ModOperatorNode($1, $3);}
          | OP_MINUS expression %prec UMINUS {$$ = new NegativeOperatorNode($2);}
          | OP_NEGATION expression {$$ = new NotOperatorNode($2);}
          | LPARENT expression RPARENT {$$ = $2;}
;


method_call: ID LPARENT expression_opt RPARENT {$$ = new MethodCallNode($1, $3);}
;


expression_opt: expression_list {$$ = $1;}
              | {$$ = NULL;}
;

expression_list: expression_list COMMA expression {$1->push_back($3); $$ = $1;}
               | expression {ExpresionList *expLst = new ExpresionList(); expLst->push_back($1); $$ = expLst;}
;


argument_list: argument_list COMMA argument {$1->push_back($3); $$ = $1;}
             | argument {ExpresionList* el = new ExpresionList(); el->push_back($1); $$ = el;}
;

argument: expression {$$ = $1;}
        | STRING {$$ = new StringNode($1);}
;


left_value_list: left_value_list COMMA left_value {$1->push_back((IdNode*)$3); $$ = $1;}
               | left_value {IdNodeList *il = new IdNodeList(); il->push_back((IdNode*)$1); $$ = il;}
;

else_block: KW_ELSE block {$$ = $2;}
          | {$$ = NULL;}
;
%%



