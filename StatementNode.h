#pragma once
#include "ExpresionNode.h"
#include "BaseType.h"
#include <iostream>
#include <list>

using namespace std;
enum StatementsType
{
ForType,
WhileType,
IfType,
BlockType,
AssignationType,
ReadType,
PrintType,
ReturnType,
ContinueType,
BreakType,
VarDeclarationType,
BlockNodeType,
ExpresionStatementNodeType
};

class StatementNode
{
public:
	virtual void ValidateSemantics()=0;
	virtual void Interpret()=0;
	virtual StatementsType GetType() = 0;
};

typedef list<StatementNode*> StatementList;

class ExpresionStatementNode;
typedef list<ExpresionStatementNode*> ExpresionStatementList;

class BlockNode: public StatementNode
{
public:
	BlockNode(StatementList* statement_list);
	~BlockNode();
	BlockNode * ParentBlock;
	StatementList *statement_list;	
	virtual void ValidateSemantics();
	virtual void Interpret();
	virtual StatementsType GetType();

private:

};

class AssignationNode : public StatementNode
{
public:
	AssignationNode(IdNode* leftValue, ExpresionNode* rightValue);
	IdNode* LeftValue;
	ExpresionNode* RightValue;
	virtual void ValidateSemantics();
	virtual void Interpret();
	virtual StatementsType GetType();

};
class ReadNode : public StatementNode
{
public:
	ReadNode(IdNodeList * id_node_list);
	~ReadNode();
	IdNodeList * id_node_list;
	virtual void ValidateSemantics();
	virtual void Interpret();
	virtual StatementsType GetType();

};

typedef list<ExpresionNode*> VarInitialForValues;
typedef list<AssignationNode*> VarFinalForValues;
typedef list<StatementNode*> VarBlock;
typedef map < string, pair<BaseType *, ExpresionValue *> > VarBlockVariables;

class ForNode : public StatementNode
{
public:
	ForNode(ExpresionList* initialExp, ExpresionNode* condition, StatementList* incrementStatement, BlockNode* block);
	~ForNode();
	ExpresionStatementList* initialExp;
	ExpresionNode* condition;
	StatementList* incrementStatement;
	BlockNode* block;
	virtual void ValidateSemantics();
	virtual void Interpret();
	virtual StatementsType GetType();


};
class PrintNode:public StatementNode
{
public:
	PrintNode(ExpresionList* expresiont_list);
	~PrintNode();
	ExpresionList* expresiont_list;
	virtual void ValidateSemantics();
	virtual void Interpret();
	virtual StatementsType GetType();

};

class IfNode :public StatementNode
{
public:
	IfNode(ExpresionNode* condition, BlockNode* block, BlockNode* elseBlock);
	~IfNode();
	ExpresionNode* Condition;
	BlockNode* Block;
	BlockNode* ElseBlock;
	virtual void ValidateSemantics();
	virtual void Interpret();
	virtual StatementsType GetType();

private:

};
class WhileNode :public StatementNode
{
public:
	WhileNode();
	WhileNode(ExpresionNode* condition, BlockNode*  block);
	~WhileNode();
	ExpresionNode* Condition;
	BlockNode*  Block;
	virtual void ValidateSemantics();
	virtual void Interpret();
	virtual StatementsType GetType();

private:

};

class ReturnNode :public StatementNode
{
public:
	ReturnNode();
	ReturnNode(ExpresionNode * expresion);
	~ReturnNode();
	ExpresionNode* Expresion;
	virtual void ValidateSemantics();
	virtual void Interpret();
	virtual StatementsType GetType();

private:

};

class BreakNode:public StatementNode	
{
public:
	BreakNode();
	~BreakNode();
	virtual void ValidateSemantics();
	virtual void Interpret();
	virtual StatementsType GetType();


};

class ContinueNode :public StatementNode
{
public:
	ContinueNode();
	~ContinueNode();
	virtual void ValidateSemantics();
	virtual void Interpret();
	virtual StatementsType GetType();


};

class VarDeclarationNode :public StatementNode
{
public:
	VarDeclarationNode(BaseType * type, string name,ExpresionNode * value);
	VarDeclarationNode(BaseType * type, list<string> *names);
	~VarDeclarationNode();
	string Name;
	list<string> *names;
	BaseType * Type;
	ExpresionNode *Value;
	virtual void ValidateSemantics();
	virtual void Interpret();
	virtual StatementsType GetType();


};

class ExpresionStatementNode : public StatementNode
{
public:
	ExpresionStatementNode(ExpresionNode * expresion_node);
	~ExpresionStatementNode();
	ExpresionNode * expresion_node;
	virtual void ValidateSemantics();
	virtual void Interpret();
	virtual StatementsType GetType();
};






