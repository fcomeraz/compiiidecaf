#include "ExpresionNode.h"


ArrayAccesor::ArrayAccesor(ExpresionNode* expresion)
{
	this->expresion = expresion;
}

ArrayAccesor::~ArrayAccesor()
{
}

BaseType*  ArrayAccesor::ValidateSemantics(BaseType* sourceType)
{
	if (typeid(*sourceType) != typeid(ArrayType*))
	{
		printf("Se necesita definir arreglo\n");
		exit(0);
	}
	ArrayType * arrayType = (ArrayType *)sourceType;

	BaseType* expresionType = expresion->ValidateSemantics();

	if(expresionType->getType() != NumberTypeClass){
		printf("El indice debe ser number");
		exit(0);
	}
	return arrayType->OfType;
}

IdNode::IdNode(string name)
{
	this->Name = name;
	this->accesor = NULL;
}

IdNode::IdNode(string name, Accesor * accesor){
	this->Name = name;
	this->accesor = accesor;
}

IdNode::~IdNode()
{
}

int IdNode::Evaluate()
{
	return 0;
}
BaseType * IdNode::ValidateSemantics()
{
	BaseType * idType = SymbolsTable::getInstance().GetVariableType(Name);
	
	if (accesor == NULL)
	{
		return idType;
	}

	return accesor->ValidateSemantics(idType);

}

ExpresionValue*  IdNode::Interpret()
{
	//TODO: tomar en cuenta el accesor
	return SymbolsTable::getInstance().GetVariableValue(Name);
}

string IdNode::ToString(){
	string ret = "IdNode: ";
	ret.append(this->Name);
	return ret;
}

NumberNode::NumberNode()
{
}

NumberNode::NumberNode(int value)
{
	this->Value = value;
}

NumberNode::NumberNode(string value){
	if(!strncmp(value.c_str(), "0x", 2)){
		if(value.length() > 10){
			printf("Constante Hexadecimal es demasiado grande para ser representada en cualquier entero\n");
			exit(0);
		}

		Value = 0;
		for(int i = 2; i < value.length(); i++){
			char c = value.c_str()[i];
			int cvalue;
			if(c >= 97 && c <= 102) cvalue = c-87;
			if(c >= 65 && c <= 70) cvalue = c-55;
			if(c >= 48 && c <= 57) cvalue = c-48;
			cvalue = cvalue << ((value.length() - (i+1))*4);
			Value = (Value | cvalue);
		}
	}else{
		this->Value = atoi(value.c_str());
	}
}

NumberNode::~NumberNode()
{
}
int NumberNode::Evaluate()
{
	return Value;
}
BaseType * NumberNode::ValidateSemantics()
{
	return new NumberType();

}
ExpresionValue*  NumberNode::Interpret()
{
	return new NumberValue(this->Value);
}

string NumberNode::ToString(){
	string ret = "NumberNode: ";
	ostringstream oss;
	oss << this->Value;
	ret.append(oss.str());
	return ret;
}

CharacterNode::CharacterNode(char value){
	this->Value = value;
}

CharacterNode::~CharacterNode(){

}

int  CharacterNode::Evaluate(){
	return this->Value;
}

BaseType * CharacterNode::ValidateSemantics(){
	return new CharacterType();
}

ExpresionValue *  CharacterNode::Interpret(){
	return new CharacterValue(this->Value);
}

string CharacterNode::ToString(){
	string ret = "CharacterNode: ";
	ostringstream oss; 
	oss << this->Value;
	ret.append(oss.str());
	return ret;
}

BoolNode::BoolNode(bool value){
	this->Value = value;
}

BoolNode::~BoolNode(){

}

int  BoolNode::Evaluate(){
	return this->Value;
}

BaseType * BoolNode::ValidateSemantics(){
	return new BoolType();
}

ExpresionValue *  BoolNode::Interpret(){
	return new BoolValue(this->Value);
}

string BoolNode::ToString(){
	string ret = "BoolNode: ";
	ret.append(this->Value?"true":"false");
	return ret;
}

StringNode::StringNode(string value){

}

StringNode::~StringNode(){

}

int StringNode::Evaluate(){
	return 0;
}

BaseType *StringNode::ValidateSemantics(){
	return new StringType();
}

ExpresionValue * StringNode::Interpret(){
	return new StringValue(this->Value);
}

string StringNode::ToString(){
	string ret =  "StringNode: ";
	ret.append(this->Value);
	return ret;
}

SumOperatorNode::SumOperatorNode()
{

}
SumOperatorNode::SumOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode)
{
	this->LeftNode = leftNode;
	this->RightNode = rightNode;

}
SumOperatorNode::~SumOperatorNode()
{
}
int SumOperatorNode::Evaluate()
{
	return LeftNode->Evaluate() + RightNode->Evaluate() ;
}
BaseType * SumOperatorNode::ValidateSemantics()
{
	BaseType* leftType = LeftNode->ValidateSemantics();
	BaseType* rightType = RightNode->ValidateSemantics();

	if (leftType->getType() != NumberTypeClass)
	{
		printf("Left Value no es Int\n");
		exit(0);
	}

	if (rightType->getType() != NumberTypeClass)
	{
		printf("Right Value no es Int\n");
		exit(0);
	}
	return new NumberType();

}
ExpresionValue*  SumOperatorNode::Interpret()
{
	ExpresionValue* leftValue = LeftNode->Interpret();
	ExpresionValue* rightValue = RightNode->Interpret();

	if (leftValue->getType() == NumberValueClass && rightValue->getType() == NumberValueClass)
	{
		return new NumberValue(((NumberValue*)leftValue)->Value + ((NumberValue*)rightValue)->Value);
	}
	return NULL;
}

string SumOperatorNode::ToString(){
	return "SumOperatorNode: ";
}

SubstractOperatorNode::SubstractOperatorNode()
{
}
SubstractOperatorNode::SubstractOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode)
{
	this->LeftNode = leftNode;
	this->RightNode = rightNode;

}
SubstractOperatorNode::~SubstractOperatorNode()
{
}
int SubstractOperatorNode::Evaluate()
{
	return LeftNode->Evaluate() - RightNode->Evaluate();
}
BaseType * SubstractOperatorNode::ValidateSemantics()
{
	BaseType* leftType = LeftNode->ValidateSemantics();
	BaseType* rightType = RightNode->ValidateSemantics();

	if (leftType->getType() != NumberTypeClass)
	{
		printf("Left Value no es Int\n");
		exit(0);
	}

	if (rightType->getType() != NumberTypeClass)
	{
		printf("Right Value no es Int\n");
		exit(0);
	}
	return new NumberType();
}

ExpresionValue*  SubstractOperatorNode::Interpret()
{
	ExpresionValue* leftValue = LeftNode->Interpret();
	ExpresionValue* rightValue = RightNode->Interpret();

	if (leftValue->getType() == NumberValueClass && rightValue->getType() == NumberValueClass)
	{
		return new NumberValue(((NumberValue*)leftValue)->Value - ((NumberValue*)rightValue)->Value);
	}
	return NULL;
}

string SubstractOperatorNode::ToString(){
	return "SubstractOperatorNode: ";
}

MultiplyOperatorNode::MultiplyOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode){
	this->LeftNode = leftNode;
	this->RightNode = rightNode;
}

MultiplyOperatorNode::~MultiplyOperatorNode(){

}

int MultiplyOperatorNode::Evaluate(){
	return LeftNode->Evaluate()*RightNode->Evaluate();
}

BaseType* MultiplyOperatorNode::ValidateSemantics(){
	BaseType* leftType = LeftNode->ValidateSemantics();
	BaseType* rightType = RightNode->ValidateSemantics();

	if (leftType->getType() != NumberTypeClass)
	{
		printf("Left Value no es Int\n");
		exit(0);
	}

	if (rightType->getType() != NumberTypeClass)
	{
		printf("Right Value no es Int\n");
		exit(0);
	}
	return new NumberType();
}

ExpresionValue * MultiplyOperatorNode::Interpret(){
	ExpresionValue* leftValue = LeftNode->Interpret();
	ExpresionValue* rightValue = RightNode->Interpret();

	if (leftValue->getType() == NumberValueClass && rightValue->getType() == NumberValueClass)
	{
		return new NumberValue(((NumberValue*)leftValue)->Value * ((NumberValue*)rightValue)->Value);
	}
	return NULL;
}

string MultiplyOperatorNode::ToString(){
	return "MultiplyOperatorNode: ";
}


DivisionOperatorNode::DivisionOperatorNode()
{
}
DivisionOperatorNode::DivisionOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode)
{
	this->LeftNode = leftNode;
	this->RightNode = rightNode;

}

DivisionOperatorNode::~DivisionOperatorNode()
{
}

int DivisionOperatorNode::Evaluate()
{
	return LeftNode->Evaluate() / RightNode->Evaluate();
}
BaseType * DivisionOperatorNode::ValidateSemantics()
{
	BaseType* leftType = LeftNode->ValidateSemantics();
	BaseType* rightType = RightNode->ValidateSemantics();

	if (leftType->getType() != NumberTypeClass)
	{
		printf("Left Value no es Int\n");
		exit(0);
	}

	if (rightType->getType() != NumberTypeClass)
	{
		printf("Right Value no es Int\n");
		exit(0);
	}
	return new NumberType();

}
ExpresionValue*  DivisionOperatorNode::Interpret()
{
	ExpresionValue* leftValue = LeftNode->Interpret();
	ExpresionValue* rightValue = RightNode->Interpret();

	if (leftValue->getType() == NumberValueClass && rightValue->getType() == NumberValueClass)
	{
		return new NumberValue(((NumberValue*)leftValue)->Value / ((NumberValue*)rightValue)->Value);
	}
	return NULL;
}

string DivisionOperatorNode::ToString(){
	return "DivisionOperatorNode: ";
}

OrOperatorNode::OrOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode)
{
	this->LeftNode = leftNode;
	this->RightNode = rightNode;
}

OrOperatorNode::~OrOperatorNode()
{

}

int OrOperatorNode::Evaluate()
{
	return (LeftNode->Evaluate() || RightNode->Evaluate()) ? 1 : 0;
}

BaseType * OrOperatorNode::ValidateSemantics()
{
	BaseType* leftType = LeftNode->ValidateSemantics();
	BaseType* rightType = RightNode->ValidateSemantics();

	if (leftType->getType() != BoolTypeClass)
	{
		printf("Left Value no es Bool\n");
		exit(0);
	}

	if (rightType->getType() != BoolTypeClass)
	{
		printf("Right Value no es Bool\n");
		exit(0);
	}
	return new BoolType();
}

ExpresionValue * OrOperatorNode::Interpret()
{
	ExpresionValue* leftValue = LeftNode->Interpret();
	ExpresionValue* rightValue = RightNode->Interpret();

	if (leftValue->getType() == BoolValueClass && rightValue->getType() == BoolValueClass)
	{
		return new BoolValue(((NumberValue*)leftValue)->Value || ((NumberValue*)rightValue)->Value);
	}
	return NULL;
}

string OrOperatorNode::ToString(){
	return "OrOperatorNode: ";
}

AndOperatorNode::AndOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode)
{
	this->LeftNode = leftNode;
	this->RightNode = rightNode;
}

AndOperatorNode::~AndOperatorNode()
{

}

int AndOperatorNode::Evaluate()
{
	return (LeftNode->Evaluate() && RightNode->Evaluate()) ? 1 : 0;
}

BaseType * AndOperatorNode::ValidateSemantics()
{
	BaseType* leftType = LeftNode->ValidateSemantics();
	BaseType* rightType = RightNode->ValidateSemantics();

	if (leftType->getType() != BoolTypeClass)
	{
		printf("Left Value no es Bool\n");
		exit(0);
	}

	if (rightType->getType() != BoolTypeClass)
	{
		printf("Right Value no es Bool\n");
		exit(0);
	}
	return new BoolType();
}

ExpresionValue * AndOperatorNode::Interpret()
{
	ExpresionValue* leftValue = LeftNode->Interpret();
	ExpresionValue* rightValue = RightNode->Interpret();

	if (leftValue->getType() == BoolValueClass && rightValue->getType() == BoolValueClass)
	{
		return new BoolValue(((NumberValue*)leftValue)->Value && ((NumberValue*)rightValue)->Value);
	}
	return NULL;
}

string AndOperatorNode::ToString(){
	return "AndOperatorNode: ";
}

GreaterEqualsThanOperatorNode::GreaterEqualsThanOperatorNode()
{
}
GreaterEqualsThanOperatorNode::GreaterEqualsThanOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode)
{
	this->LeftNode = leftNode;
	this->RightNode = rightNode;

}
GreaterEqualsThanOperatorNode::~GreaterEqualsThanOperatorNode()
{
}
int GreaterEqualsThanOperatorNode::Evaluate()
{
	return (LeftNode->Evaluate() >= RightNode->Evaluate()) ? 1 : 0;
}

BaseType * GreaterEqualsThanOperatorNode::ValidateSemantics()
{
	BaseType* leftType = LeftNode->ValidateSemantics();
	BaseType* rightType = RightNode->ValidateSemantics();

	if (leftType->getType() != NumberTypeClass)
	{
		printf("Left Value no es Int\n");
		exit(0);
	}

	if (rightType->getType() != NumberTypeClass)
	{
		printf("Right Value no es Int\n");
		exit(0);
	}
	return new BoolType();

}
ExpresionValue*  GreaterEqualsThanOperatorNode::Interpret()
{
	ExpresionValue* leftValue = LeftNode->Interpret();
	ExpresionValue* rightValue = RightNode->Interpret();

	if (leftValue->getType() == NumberValueClass && rightValue->getType() == NumberValueClass)
	{
		return new BoolValue(((NumberValue*)leftValue)->Value >= ((NumberValue*)rightValue)->Value);
	}
	return NULL;
}

string GreaterEqualsThanOperatorNode::ToString(){
	return "GreaterEqualsThanOperatorNode: ";
}

GreaterThanOperatorNode::GreaterThanOperatorNode()
{
}
GreaterThanOperatorNode::GreaterThanOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode)
{
	this->LeftNode = leftNode;
	this->RightNode = rightNode;

}
GreaterThanOperatorNode::~GreaterThanOperatorNode()
{
}
int GreaterThanOperatorNode::Evaluate()
{
	return (LeftNode->Evaluate() > RightNode->Evaluate()) ? 1 : 0;
}
BaseType * GreaterThanOperatorNode::ValidateSemantics()
{
	BaseType* leftType = LeftNode->ValidateSemantics();
	BaseType* rightType = RightNode->ValidateSemantics();

	if (leftType->getType() != NumberTypeClass)
	{
		printf("Left Value no es Int\n");
		exit(0);
	}

	if (rightType->getType() != NumberTypeClass)
	{
		printf("Right Value no es Int\n");
		exit(0);
	}
	return new BoolType();

}
ExpresionValue*  GreaterThanOperatorNode::Interpret()
{
	ExpresionValue* leftValue = LeftNode->Interpret();
	ExpresionValue* rightValue = RightNode->Interpret();

	if (leftValue->getType() == NumberValueClass && rightValue->getType() == NumberValueClass)
	{
		return new BoolValue(((NumberValue*)leftValue)->Value > ((NumberValue*)rightValue)->Value);
	}
	return NULL;
}

string GreaterThanOperatorNode::ToString(){
	return "GreaterThanOperatorNode: ";
}


LessEqualsThanOperatorNode::LessEqualsThanOperatorNode()
{
}
LessEqualsThanOperatorNode::LessEqualsThanOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode)
{
	this->LeftNode = leftNode;
	this->RightNode = rightNode;

}
LessEqualsThanOperatorNode::~LessEqualsThanOperatorNode()
{
}
int LessEqualsThanOperatorNode::Evaluate()
{
	return (LeftNode->Evaluate() <= RightNode->Evaluate()) ? 1 : 0;
}
BaseType * LessEqualsThanOperatorNode::ValidateSemantics()
{
	BaseType* leftType = LeftNode->ValidateSemantics();
	BaseType* rightType = RightNode->ValidateSemantics();

	if (leftType->getType() != NumberTypeClass)
	{
		printf("Left Value no es Int\n");
		exit(0);
	}

	if (rightType->getType() != NumberTypeClass)
	{
		printf("Right Value no es Int\n");
		exit(0);
	}
	return new BoolType();

}
ExpresionValue*  LessEqualsThanOperatorNode::Interpret()
{
	ExpresionValue* leftValue = LeftNode->Interpret();
	ExpresionValue* rightValue = RightNode->Interpret();

	if (leftValue->getType() == NumberValueClass && rightValue->getType() == NumberValueClass)
	{
		return new BoolValue(((NumberValue*)leftValue)->Value <= ((NumberValue*)rightValue)->Value);
	}
	return NULL;
}

string LessEqualsThanOperatorNode::ToString(){
	return "LessEqualsThanOperatorNode: ";
}

LessThanOperatorNode::LessThanOperatorNode()
{
}
LessThanOperatorNode::LessThanOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode)
{
	this->LeftNode = leftNode;
	this->RightNode = rightNode;

}
LessThanOperatorNode::~LessThanOperatorNode()
{
}
int LessThanOperatorNode::Evaluate()
{
	return (LeftNode->Evaluate() < RightNode->Evaluate()) ? 1 : 0;
}
BaseType * LessThanOperatorNode::ValidateSemantics()
{
	BaseType* leftType = LeftNode->ValidateSemantics();
	BaseType* rightType = RightNode->ValidateSemantics();

	if (leftType->getType() != NumberTypeClass)
	{
		printf("Left Value no es Int\n");
		exit(0);
	}

	if (rightType->getType() != NumberTypeClass)
	{
		printf("Right Value no es Int\n");
		exit(0);
	}
	return new BoolType();

}
ExpresionValue*  LessThanOperatorNode::Interpret()
{
	ExpresionValue* leftValue = LeftNode->Interpret();
	ExpresionValue* rightValue = RightNode->Interpret();

	if (leftValue->getType() == NumberValueClass && rightValue->getType() == NumberValueClass)
	{
		return new BoolValue(((NumberValue*)leftValue)->Value < ((NumberValue*)rightValue)->Value);
	}
	return NULL;
}

string LessThanOperatorNode::ToString(){
	return "LessThanOperatorNode: ";
}


EqualsOperatorNode::EqualsOperatorNode()
{

}

EqualsOperatorNode::EqualsOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode)
{
	this->LeftNode = leftNode;
	this->RightNode = rightNode;
	
}
EqualsOperatorNode::~EqualsOperatorNode()
{
}
int EqualsOperatorNode::Evaluate()
{
	return LeftNode->Evaluate() == RightNode->Evaluate() ? 1 : 0;
}
BaseType * EqualsOperatorNode::ValidateSemantics()
{
	BaseType* leftType = LeftNode->ValidateSemantics();
	BaseType* rightType = RightNode->ValidateSemantics();

	if(leftType->getType() != NumberTypeClass && leftType->getType() != BoolTypeClass){
		printf("left Value no es ni Int ni Bool\n");
		exit(0);
	}

	if(leftType->getType() == NumberTypeClass){
		if (rightType->getType() != NumberTypeClass)
		{
			printf("Right Value no es Int\n");
			exit(0);
		}
	}

	if(leftType->getType() == BoolTypeClass){
		if (rightType->getType() != BoolTypeClass)
		{
			printf("Right Value no es Bool\n");
			exit(0);
		}
	}
	
	return new BoolType();

}
ExpresionValue*  EqualsOperatorNode::Interpret()
{
	ExpresionValue* leftValue = LeftNode->Interpret();
	ExpresionValue* rightValue = RightNode->Interpret();

	if (leftValue->getType() == NumberValueClass && rightValue->getType() == NumberValueClass)
	{
		return new BoolValue(((NumberValue*)leftValue)->Value == ((NumberValue*)rightValue)->Value);
	}
	if (leftValue->getType() == BoolValueClass && rightValue->getType() == BoolValueClass)
	{
		return new BoolValue(((BoolValue*)leftValue)->Value == ((BoolValue*)rightValue)->Value);
	}
	return NULL;
}

string EqualsOperatorNode::ToString(){
	return "EqualsOperatorNode: ";
}


NotEqualsOperatorNode::NotEqualsOperatorNode()
{
}
NotEqualsOperatorNode::NotEqualsOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode)
{
	this->LeftNode = leftNode;
	this->RightNode = rightNode;

}
NotEqualsOperatorNode::~NotEqualsOperatorNode()
{
}
int NotEqualsOperatorNode::Evaluate()
{
	return (LeftNode->Evaluate() != RightNode->Evaluate()) ? 1 : 0;
}
BaseType * NotEqualsOperatorNode::ValidateSemantics()
{
	BaseType* leftType = LeftNode->ValidateSemantics();
	BaseType* rightType = RightNode->ValidateSemantics();

	if(leftType->getType() != NumberTypeClass && leftType->getType() != BoolTypeClass){
		printf("left Value no es ni Int ni Bool\n");
		exit(0);
	}

	if(leftType->getType() == NumberTypeClass){
		if (rightType->getType() != NumberTypeClass)
		{
			printf("Right Value no es Int\n");
			exit(0);
		}
	}

	if(leftType->getType() == BoolTypeClass){
		if (rightType->getType() != BoolTypeClass)
		{
			printf("Right Value no es Bool\n");
			exit(0);
		}
	}
	
	return new BoolType();

}
ExpresionValue*  NotEqualsOperatorNode::Interpret()
{
	ExpresionValue* leftValue = LeftNode->Interpret();
	ExpresionValue* rightValue = RightNode->Interpret();

	if (leftValue->getType() == NumberValueClass && rightValue->getType() == NumberValueClass)
	{
		return new BoolValue(((NumberValue*)leftValue)->Value != ((NumberValue*)rightValue)->Value);
	}
	if (leftValue->getType() == BoolValueClass && rightValue->getType() == BoolValueClass)
	{
		return new BoolValue(((BoolValue*)leftValue)->Value != ((BoolValue*)rightValue)->Value);
	}
	return NULL;
}

string NotEqualsOperatorNode::ToString(){
	return "NotEqualsOperatorNode: ";
}

LeftShiftOperatorNode::LeftShiftOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode){
	this->LeftNode = leftNode;
	this->RightNode = rightNode;
}

LeftShiftOperatorNode::~LeftShiftOperatorNode(){

}	

int LeftShiftOperatorNode::Evaluate(){
	int xs = LeftNode->Evaluate();
	unsigned int x = 0 | xs; 
	int y = RightNode->Evaluate();
	int z = 0 | (x << y);
	return z;
}

BaseType * LeftShiftOperatorNode::ValidateSemantics(){
	BaseType* leftType = LeftNode->ValidateSemantics();
	BaseType* rightType = RightNode->ValidateSemantics();

	if (leftType->getType() != NumberTypeClass)
	{
		printf("Left Value no es Int\n");
		exit(0);
	}

	if (rightType->getType() != NumberTypeClass)
	{
		printf("Right Value no es Int\n");
		exit(0);
	}
	return new NumberType();
}

ExpresionValue * LeftShiftOperatorNode::Interpret(){
	ExpresionValue* leftValue = LeftNode->Interpret();
	ExpresionValue* rightValue = RightNode->Interpret();

	if (leftValue->getType() == NumberValueClass && rightValue->getType() == NumberValueClass)
	{
		int xs = ((NumberValue*)leftValue)->Value;
		unsigned int x = 0 | xs;
		int y = ((NumberValue*)rightValue)->Value;
		int z = 0 | (x<<y);
		return new NumberValue(z);
	}
	return NULL;
}

string LeftShiftOperatorNode::ToString(){
	return "LeftShiftOperatorNode: ";
}

RightShiftOperatorNode::RightShiftOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode){
	this->LeftNode = leftNode;
	this->RightNode = rightNode;
}

RightShiftOperatorNode::~RightShiftOperatorNode(){

}

int RightShiftOperatorNode::Evaluate(){
	int xs = LeftNode->Evaluate();
	unsigned int x = 0 | xs; 
	int y = RightNode->Evaluate();
	int z = 0 | (x >> y);
	return z;
}

BaseType * RightShiftOperatorNode::ValidateSemantics(){
	BaseType* leftType = LeftNode->ValidateSemantics();
	BaseType* rightType = RightNode->ValidateSemantics();

	if (leftType->getType() != NumberTypeClass)
	{
		printf("Left Value no es Int\n");
		exit(0);
	}

	if (rightType->getType() != NumberTypeClass)
	{
		printf("Right Value no es Int\n");
		exit(0);
	}
	return new NumberType();
}

ExpresionValue * RightShiftOperatorNode::Interpret(){
	ExpresionValue* leftValue = LeftNode->Interpret();
	ExpresionValue* rightValue = RightNode->Interpret();

	if (leftValue->getType() == NumberValueClass && rightValue->getType() == NumberValueClass)
	{
		int xs = ((NumberValue*)leftValue)->Value;
		unsigned int x = 0 | xs;
		int y = ((NumberValue*)rightValue)->Value;
		int z = 0 | (x>>y);
		return new NumberValue(z);
	}
	return NULL;
}

string RightShiftOperatorNode::ToString(){
	return "RightShiftOperatorNode: ";
}

RotOperatorNode::RotOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode){
	this->LeftNode = leftNode;
	this->RightNode = rightNode;
}

RotOperatorNode::~RotOperatorNode(){

}

int RotOperatorNode::Evaluate(){
	int xs = LeftNode->Evaluate();
	unsigned x = 0 | xs;
	int y = RightNode->Evaluate();
	int z = 0;
	if(y >= 0){
		z = x << y | x >> (sizeof(int)*8-y);
	}else{
		z = x >> abs(y) | x << (sizeof(int)*8+y);
	}

	return z;
}

BaseType * RotOperatorNode::ValidateSemantics(){
	BaseType* leftType = LeftNode->ValidateSemantics();
	BaseType* rightType = RightNode->ValidateSemantics();

	if (leftType->getType() != NumberTypeClass)
	{
		printf("Left Value no es Int\n");
		exit(0);
	}

	if (rightType->getType() != NumberTypeClass)
	{
		printf("Right Value no es Int\n");
		exit(0);
	}
	return new NumberType();
}

ExpresionValue * RotOperatorNode::Interpret(){
	ExpresionValue* leftValue = LeftNode->Interpret();
	ExpresionValue* rightValue = RightNode->Interpret();

	if (leftValue->getType() == NumberValueClass && rightValue->getType() == NumberValueClass)
	{
		int xs = ((NumberValue*)leftValue)->Value;
		unsigned int x = 0 | xs;
		int y = ((NumberValue*)rightValue)->Value;
		int z = 0;
		if(y >= 0){
			z = x << y | x >> (sizeof(int)*8-y);
		}else{
			z = x >> abs(y) | x << (sizeof(int)*8+y);
		}
		return new NumberValue(z);
	}
	return NULL;
}

string RotOperatorNode::ToString(){
	return "RotOperatorNode: ";
}



ModOperatorNode::ModOperatorNode()
{
}
ModOperatorNode::ModOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode)
{
	this->LeftNode = leftNode;
	this->RightNode = rightNode;

}
ModOperatorNode::~ModOperatorNode()
{
}
int ModOperatorNode::Evaluate()
{
	return (LeftNode->Evaluate() % RightNode->Evaluate());
}
BaseType * ModOperatorNode::ValidateSemantics()
{
	BaseType* leftType = LeftNode->ValidateSemantics();
	BaseType* rightType = RightNode->ValidateSemantics();

	if (leftType->getType() != NumberTypeClass){
		printf("left Value no es ni Int \n");
		exit(0);
	}


	if (rightType->getType() != NumberTypeClass)
	{
		printf("Right Value no es Int\n");
		exit(0);
	}

	return new NumberType();

}
ExpresionValue*  ModOperatorNode::Interpret()
{
	ExpresionValue* leftValue = LeftNode->Interpret();
	ExpresionValue* rightValue = RightNode->Interpret();

	if (leftValue->getType() == NumberValueClass && rightValue->getType() == NumberValueClass)
	{
		return new NumberValue(((NumberValue*)leftValue)->Value % ((NumberValue*)rightValue)->Value);
	}
	return NULL;
}

string ModOperatorNode::ToString(){
	return "ModOperatorNode: ";
}


NegativeOperatorNode::NegativeOperatorNode()
{
}
NegativeOperatorNode::NegativeOperatorNode(ExpresionNode* expresion)
{
	this->node = expresion;

}
NegativeOperatorNode::~NegativeOperatorNode()
{
}
int NegativeOperatorNode::Evaluate()
{
	return (node->Evaluate());
}
BaseType * NegativeOperatorNode::ValidateSemantics()
{
	BaseType* nodeType = node->ValidateSemantics();

	if (nodeType->getType() != NumberTypeClass){
		printf("Value no es ni Int \n");
		exit(0);
	}

	return new NumberType();

}
ExpresionValue*  NegativeOperatorNode::Interpret()
{
	ExpresionValue* nodeValue = node->Interpret();

	if (nodeValue->getType() == NumberValueClass)
	{
		return new NumberValue(((NumberValue*)nodeValue)->Value*-1);
	}
	return NULL;
}

string NegativeOperatorNode::ToString(){
	return "NegativeOperatorNode: ";
}


NotOperatorNode::NotOperatorNode()
{
}
NotOperatorNode::NotOperatorNode(ExpresionNode* expresion)
{
	this->node = expresion;

}
NotOperatorNode::~NotOperatorNode()
{
}
int NotOperatorNode::Evaluate()
{
	return (node->Evaluate());
}
BaseType * NotOperatorNode::ValidateSemantics()
{
	BaseType* nodeType = node->ValidateSemantics();

	if (nodeType->getType() != BoolTypeClass){
		printf("Value no es ni Bool \n");
		exit(0);
	}

	return new NumberType();

}
ExpresionValue*  NotOperatorNode::Interpret()
{
	ExpresionValue* nodeValue = node->Interpret();

	if (nodeValue->getType() == BoolValueClass)
	{
		return new BoolValue(!((BoolValue*)nodeValue)->Value);
	}
	return NULL;
}

string NotOperatorNode::ToString(){
	return "NotOperatorNode: ";
}


MethodCallNode::MethodCallNode(string id, ExpresionList* arguments){
	this->id = id;
	this->arguments = arguments;
}

MethodCallNode::~MethodCallNode(){

}

int MethodCallNode::Evaluate(){
	//TODO: implementar
	printf("Implementar el evaluate de MethodCallNode\n");
	exit(0);
}

BaseType * MethodCallNode::ValidateSemantics(){
	//TODO: implementar
	printf("Revisar que el metodo exista\n");
	printf("Revisar que los parametros sean correctos\n");
	printf("Devolver el tipo de la funcion\n");
	exit(0);
}

ExpresionValue * MethodCallNode::Interpret(){
	//TODO: implementar
	printf("Ejecutar el methodo");
	printf("Devolver el valor correspondiente");
	exit(0);
}

string MethodCallNode::ToString(){
	string ret = "MethodCallNode{ Id: ";
	ret.append(this->id);
	ret.append("}");
	return ret;
}




























