#pragma once

#include <map>
#include <stdlib.h>
#include "BaseType.h"
#include "ExpresionValue.h"
#include <list>

using namespace std;

typedef map<string, BaseType *> VarTypes;
typedef map<string, ExpresionValue *> VarValues;
class Function
{
public:
	Function();
	~Function();

};


class SymbolsTable
{
public:
	static SymbolsTable& getInstance();
	~SymbolsTable();
	void DeclareVariable(string id, BaseType * type);
	BaseType * GetVariableType(string id);
	void SetVariableValue(string id, ExpresionValue *value);
	ExpresionValue * GetVariableValue(string id);

private:
	VarTypes _variablesType ;
	VarValues _variablesValue;	
	SymbolsTable();
	static SymbolsTable* instance; 
};

