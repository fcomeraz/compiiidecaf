#pragma once
#include "ExpresionValue.h"
#include <string>
#include <list>
#include <typeinfo>

using namespace std;

enum TypeClassType
{
	ArrayTypeClass,
	BoolTypeClass,
	NumberTypeClass,
	StringTypeClass,
	CharacterTypeClass
};

class BaseType
{
public:
	virtual bool  IsAssignable(BaseType * type)=0;
	virtual ExpresionValue * GetDefaultValue()=0;
	virtual ExpresionValue * Parse(string inputValue)=0;
	virtual TypeClassType getType() = 0;
};


typedef list<int> VarDimensions;

class ArrayType :
	public BaseType
{
public:
	ArrayType();
	~ArrayType();
	VarDimensions Dimensions;
	BaseType * OfType;
	virtual bool IsAssignable(BaseType * type);
	virtual ExpresionValue * GetDefaultValue();
	virtual ExpresionValue * Parse(string inputValue);
	virtual TypeClassType getType();
};


class BoolType :
	public BaseType
{
public:
	BoolType();
	~BoolType();
	bool Value;
	virtual bool IsAssignable(BaseType * type);
	virtual ExpresionValue * GetDefaultValue();
	virtual ExpresionValue * Parse(string inputValue);
	virtual TypeClassType getType();

};

class NumberType :
	public  BaseType
{
public:
	NumberType();
	~NumberType();
	virtual bool IsAssignable(BaseType * type);
	virtual ExpresionValue * GetDefaultValue();
	virtual ExpresionValue * Parse(string inputValue);
	virtual TypeClassType getType();

};

class CharacterType : 
	public BaseType
{
public:
	CharacterType();
	~CharacterType();
	virtual bool IsAssignable(BaseType * type);
	virtual ExpresionValue * GetDefaultValue();
	virtual ExpresionValue * Parse(string inputValue);
	virtual TypeClassType getType();	
};

class StringType :
	public BaseType
{
public:
	StringType();
	~StringType();
	virtual bool IsAssignable(BaseType * type);
	virtual ExpresionValue * GetDefaultValue();
	virtual ExpresionValue * Parse(string inputValue);
	virtual TypeClassType getType();
};
