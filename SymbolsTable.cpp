#include "SymbolsTable.h"

SymbolsTable& SymbolsTable::getInstance(){
	static SymbolsTable instance;
	return instance;
	
}

SymbolsTable::SymbolsTable()
{
}

void SymbolsTable::DeclareVariable(string id, BaseType * type)
{
	if (_variablesType.find(id) != _variablesType.end())
	{
		printf("Ya esta declarado el id %s \n", id.c_str());
		exit(0);
	}
	_variablesType[id] = type;
	_variablesValue[id] = type->GetDefaultValue();
}

BaseType * SymbolsTable::GetVariableType(string id)
{
	if (_variablesType.find(id) == _variablesType.end())
	{
		printf("La variable %s no existe\n", id.c_str());
		exit(0);
	}
	return _variablesType[id];

}

void SymbolsTable::SetVariableValue(string id, ExpresionValue * value)
{
	_variablesValue[id] = value;
}

ExpresionValue * SymbolsTable::GetVariableValue(string id)
{
	return _variablesValue[id];
}

SymbolsTable::~SymbolsTable()
{
}

Function::Function()
{
}

Function::~Function()
{
}