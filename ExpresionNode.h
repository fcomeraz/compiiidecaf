#pragma once
#include "BaseType.h"
#include <list>
#include <string>
#include "SymbolsTable.h"
#include <cmath>
#include <iostream>

using namespace std;


class Accesor
{
public:
	virtual BaseType* ValidateSemantics(BaseType* sourceType) = 0;

};

class ExpresionNode
{
public:
	virtual int Evaluate() = 0;
	virtual BaseType * ValidateSemantics() = 0;
	virtual ExpresionValue * Interpret() = 0;
	virtual string ToString() = 0;
};


typedef list<ExpresionNode*> VarExpresionIndexes;

typedef list<ExpresionNode*> ExpresionList;

class IdNode;
typedef list<IdNode*> IdNodeList;

class ArrayAccesor : public Accesor
{
public:
	ArrayAccesor(ExpresionNode *expresion);
	~ArrayAccesor();
	ExpresionNode* expresion;
	virtual BaseType* ValidateSemantics(BaseType* sourceType);

};

class IdNode:public ExpresionNode
{
public:
	IdNode();
	IdNode(string name);
	IdNode(string name, Accesor *accesor);
	~IdNode();
	string Name;
	Accesor* accesor;
	virtual int Evaluate();
	virtual BaseType * ValidateSemantics();
	virtual ExpresionValue * Interpret();
	virtual string ToString();
};

class NumberNode :public ExpresionNode
{
public:
	NumberNode();
	NumberNode(int value);
	NumberNode(string value);
	~NumberNode();
	int Value;
	virtual int Evaluate();
	virtual BaseType * ValidateSemantics();
	virtual ExpresionValue * Interpret();
	virtual string ToString();

};

class CharacterNode :public ExpresionNode
{
public:
	CharacterNode(char value);
	~CharacterNode();
	char Value;
	virtual int Evaluate();
	virtual BaseType *ValidateSemantics();
	virtual ExpresionValue * Interpret();
	virtual string ToString();
};

class BoolNode : public ExpresionNode
{
public:
	BoolNode(bool value);
	~BoolNode();
	bool Value;
	virtual int Evaluate();
	virtual BaseType *ValidateSemantics();
	virtual ExpresionValue * Interpret();
	virtual string ToString();
};

class StringNode : public ExpresionNode
{
public:
	StringNode();
	StringNode(string value);
	~StringNode();
	string Value;
	virtual int Evaluate();
	virtual BaseType *ValidateSemantics();
	virtual ExpresionValue * Interpret();
	virtual string ToString();
};

class BinaryOperatorNode : public ExpresionNode
{
public:
	ExpresionNode* LeftNode;
	ExpresionNode* RightNode;
	virtual int Evaluate()=0;
	virtual BaseType * ValidateSemantics()=0;
	virtual ExpresionValue * Interpret()=0;
	virtual string ToString()=0;

};

class UnaryOperatorNode : public ExpresionNode
{
public:
	ExpresionNode* node;
	virtual int Evaluate()=0;
	virtual BaseType * ValidateSemantics()=0;
	virtual ExpresionValue * Interpret()=0;
	virtual string ToString()=0;
};

class SumOperatorNode :public BinaryOperatorNode
{
public:
	SumOperatorNode();
	SumOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode);
	~SumOperatorNode();
	virtual int Evaluate();
	virtual BaseType * ValidateSemantics();
	virtual ExpresionValue * Interpret();
	virtual string ToString();


};

class SubstractOperatorNode :public BinaryOperatorNode
{
public:
	SubstractOperatorNode();
	SubstractOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode);
	~SubstractOperatorNode();
	virtual int Evaluate();
	virtual BaseType * ValidateSemantics();
	virtual ExpresionValue * Interpret();
	virtual string ToString();
};

class MultiplyOperatorNode : public BinaryOperatorNode
{
public:
	MultiplyOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode);
	~MultiplyOperatorNode();
	virtual int Evaluate();
	virtual BaseType* ValidateSemantics();
	virtual ExpresionValue * Interpret();
	virtual string ToString();
};

class DivisionOperatorNode :public BinaryOperatorNode
{
public:
	DivisionOperatorNode();
	DivisionOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode);
	~DivisionOperatorNode();
	virtual int Evaluate();
	virtual BaseType * ValidateSemantics();
	virtual ExpresionValue * Interpret();
	virtual string ToString();
};

class OrOperatorNode : public BinaryOperatorNode
{
public:
	OrOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode);
	~OrOperatorNode();
	virtual int Evaluate();
	virtual BaseType * ValidateSemantics();
	virtual ExpresionValue * Interpret();
	virtual string ToString();
};

class AndOperatorNode : public BinaryOperatorNode
{
public:
	AndOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode);
	~AndOperatorNode();	
	virtual int Evaluate();
	virtual BaseType * ValidateSemantics();
	virtual ExpresionValue * Interpret();
	virtual string ToString();
};

class GreaterEqualsThanOperatorNode :public BinaryOperatorNode
{
public:
	GreaterEqualsThanOperatorNode();
	GreaterEqualsThanOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode);
	~GreaterEqualsThanOperatorNode();
	virtual int Evaluate();
	virtual BaseType * ValidateSemantics();
	virtual ExpresionValue * Interpret();
	virtual string ToString();


};
class GreaterThanOperatorNode :public BinaryOperatorNode
{
public:
	GreaterThanOperatorNode();
	GreaterThanOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode);
	~GreaterThanOperatorNode();
	virtual int Evaluate();
	virtual BaseType * ValidateSemantics();
	virtual ExpresionValue * Interpret();
	virtual string ToString();
};

class LessEqualsThanOperatorNode :public BinaryOperatorNode
{
public:
	LessEqualsThanOperatorNode();
	LessEqualsThanOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode);
	~LessEqualsThanOperatorNode();
	virtual int Evaluate();
	virtual BaseType * ValidateSemantics();
	virtual ExpresionValue * Interpret();
	virtual string ToString();
};

class LessThanOperatorNode :public BinaryOperatorNode
{
public:
	LessThanOperatorNode();
	LessThanOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode);
	~LessThanOperatorNode();
	virtual int Evaluate();
	virtual BaseType * ValidateSemantics();
	virtual ExpresionValue * Interpret();
	virtual string ToString();
};

class EqualsOperatorNode :public BinaryOperatorNode
{
public:
	EqualsOperatorNode();
	EqualsOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode);
	~EqualsOperatorNode();
	virtual int Evaluate();
	virtual BaseType * ValidateSemantics();
	virtual ExpresionValue * Interpret();
	virtual string ToString();


};

class NotEqualsOperatorNode :public BinaryOperatorNode
{
public:
	NotEqualsOperatorNode();
	NotEqualsOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode);
	~NotEqualsOperatorNode();
	virtual int Evaluate();
	virtual BaseType * ValidateSemantics();
	virtual ExpresionValue * Interpret();
	virtual string ToString();


};

class ModOperatorNode :public BinaryOperatorNode
{
public:
	ModOperatorNode();
	ModOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode);
	~ModOperatorNode();
	virtual int Evaluate();
	virtual BaseType * ValidateSemantics();
	virtual ExpresionValue * Interpret();
	virtual string ToString();
};

class NotOperatorNode :public UnaryOperatorNode
{
public:
	NotOperatorNode();
	NotOperatorNode(ExpresionNode* expresion);
	~NotOperatorNode();
	virtual int Evaluate();
	virtual BaseType * ValidateSemantics();
	virtual ExpresionValue * Interpret();
	virtual string ToString();
};

class NegativeOperatorNode :public UnaryOperatorNode
{
public:
	NegativeOperatorNode();
	NegativeOperatorNode(ExpresionNode* expresion);
	~NegativeOperatorNode();
	virtual int Evaluate();
	virtual BaseType * ValidateSemantics();
	virtual ExpresionValue * Interpret();
	virtual string ToString();
};

class LeftShiftOperatorNode : public BinaryOperatorNode
{
public:
	LeftShiftOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode);
	~LeftShiftOperatorNode();	
	virtual int Evaluate();
	virtual BaseType * ValidateSemantics();
	virtual ExpresionValue * Interpret();
	virtual string ToString();
};

class RightShiftOperatorNode : public BinaryOperatorNode
{
public:
	RightShiftOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode);
	~RightShiftOperatorNode();
	virtual int Evaluate();
	virtual BaseType * ValidateSemantics();
	virtual ExpresionValue * Interpret();
	virtual string ToString();
};

class RotOperatorNode : public BinaryOperatorNode
{
public:
	RotOperatorNode(ExpresionNode* leftNode, ExpresionNode* rightNode);
	~RotOperatorNode();
	virtual int Evaluate();
	virtual BaseType * ValidateSemantics();
	virtual ExpresionValue * Interpret();
	virtual string ToString();
};

class MethodCallNode : public ExpresionNode
{
public:
	MethodCallNode(string id, ExpresionList* arguments);
	~MethodCallNode();
	string id;
	ExpresionList* arguments;
	virtual int Evaluate();
	virtual BaseType * ValidateSemantics();
	virtual ExpresionValue * Interpret();
	virtual string ToString();
};

