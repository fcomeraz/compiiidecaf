%option noyywrap


%{
	#include <iostream>
	#include <stdio.h>
	#include <String>
	#include "ExpresionValue.h"	
	#include "ExpresionNode.h"
	#include "StatementNode.h"
	#include "tokens.h"

	using namespace std;

	string cadena;
	int line = 1;


	extern ExpresionList *result;
%}

id [a-zA-Z_][a-zA-Z_0-9]*
decimal (0|[1-9][0-9]*)
hexadecimal "0x"[0-9a-fA-F]+
character \'\'

%x string_literal
%x character

%%
\" {cadena = ""; BEGIN(string_literal);}
\' {cadena = ""; BEGIN(character);}
"bool" {return KW_BOOL;}
"break" {return KW_BREAK;}
"print" {return KW_PRINT;}
"read" {return KW_READ;}
"continue" {return KW_CONTINUE;}
"class" {return KW_CLASS;}
"else" {return KW_ELSE;}
"extends" {return KW_EXTENDS;}
"false" {return KW_FALSE;}
"for" {return KW_FOR;}
"if" {return KW_IF;}
"int" {return KW_INT;}
"new" {return KW_NEW;}
"null" {return KW_NULL;}
"return" {return KW_RETURN;}
"rot" {return OP_ROT;}
"true" {return KW_TRUE;}
"void" {return KW_VOID;}
"while" {return KW_WHILE;}
{id} {yylval.s_id = strdup(yytext); return ID;}
{decimal} {yylval.i_dec = atoi(yytext); return INT;}
{hexadecimal} {yylval.s_hex = strdup(yytext); return HEX;}
{character} {yylval.c_character = yytext[0];}
"{" {return LCURLY;}
"}" {return RCURLY;}
"[" {return LSQUARE;}
"]" {return RSQUARE;}
"," {return COMMA;}
";" {return SEMICOLON;}
"(" {return LPARENT;}
")" {return RPARENT;}
"=" {return OP_ASSIGN;}
"-" {return OP_MINUS;}
"!" {return OP_NEGATION;}
"+" {return OP_PLUS;}
"*" {return OP_MULTIPLY;}
"/" {return OP_DIVISION;}
">>" {return OP_RSHIFT;}
"<<" {return OP_LSHIFT;}
"<" {return OP_LESS;}
">" {return OP_GREATER;}
"%" {return OP_MOD;}
"<=" {return OP_LESSEQUAL;}
">=" {return OP_GREATEREQUAL;}
"==" {return OP_EQUAL;}
"!=" {return OP_NOTEQUAL;}
"&&" {return OP_AND;}
"||" {return OP_OR;}
\. {return OP_DOT;}
[\n] {line++;}
[ \t\r\s]
"//"[^\n]*
. {printf("\nCaracter '%s' no es valido\n", yytext); exit(0);}
<string_literal>\" {BEGIN(INITIAL); yylval.s_string = strdup(cadena.c_str()); return STRING;}
<string_literal>\\[t] {cadena.append("\t");}
<string_literal>\\[r] {cadena.append("\r");}
<string_literal>\\[n] {cadena.append("\n");}
<string_literal>\\[a] {cadena.append("\a");}
<string_literal>\\[\\] {cadena.append("\\");}
<string_literal>\\[\"] {cadena.append("\"");}
<string_literal>\\[^trna\\\"] {printf("\nError se esperaba un caracter valido de escape\n"); exit(0);}
<string_literal>[^\n] {cadena.append(yytext);}
<string_literal>[\n] {printf("\nError se esperaba cierre de cadena \"\n"); exit(0);}
<string_literal><<EOF>> {printf("Error se esperaba cierre de cadena \"\n"); exit(0);}
<character>\\[t]\' {BEGIN(INITIAL); yylval.c_character = '\t'; return CHARACTER;}
<character>\\[r]\' {BEGIN(INITIAL); yylval.c_character = '\r'; return CHARACTER;}
<character>\\[n]\' {BEGIN(INITIAL); yylval.c_character = '\n'; return CHARACTER;}
<character>\\[a]\' {BEGIN(INITIAL); yylval.c_character = '\a'; return CHARACTER;}
<character>\\[\\]\' {BEGIN(INITIAL); yylval.c_character = '\\'; return CHARACTER;}
<character>\\[\"]\' {BEGIN(INITIAL); yylval.c_character = '\"'; return CHARACTER;}
<character>\\[\']\' {BEGIN(INITIAL); yylval.c_character = '\''; return CHARACTER;}
<character>\\[^trna\\\"] {printf("\nError se esperaba un caracter valido de escape\n"); exit(0);}
<character>[\40-\176]\' {BEGIN(INITIAL); yylval.c_character = yytext[0]; return CHARACTER;}
<character>[\40-\176][^\'] {printf("Se esperaba un fin de character \'\n"); exit(0);}
<character>. {printf("\nError se esperaba un caracter ascii imprimible entre 32 y 126\n"); exit(0);}
%%

int main(){
	yyparse();
	if(result != NULL){
		cout << "ExpCount: " << result->size() << endl;
	    for (ExpresionList::iterator i = result->begin(); i != result->end(); ++i)
    	{
    		cout << (*i)->ToString() << endl;
    		if((*i)->ValidateSemantics()){
				cout << (*i)->Interpret()->ToString() << endl;
			}	
    	}
	}
	return 0;
}




















