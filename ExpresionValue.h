#pragma once
#include <string>
#include <list>
#include <iomanip>
#include <algorithm>
#include <cctype>
#include <sstream>
#include <string>
#include <stdlib.h>
#include <cmath>

using namespace std;

enum ExpresionClassType
{
	NumberValueClass,
	BoolValueClass,
	StringValueClass,
	ArrayValueClass,
	CharacterValueClass
};

class ExpresionValue
{
public:
	virtual ExpresionClassType getType() = 0;
	virtual string ToString() = 0;
};

class ArrayValue :
	public ExpresionValue
{
public:
	ArrayValue();
	ArrayValue(list<ExpresionValue *> value);
	~ArrayValue();
	list<ExpresionValue*> Value;
	virtual ExpresionClassType getType();
	virtual string ToString();
};

class BoolValue :
	public ExpresionValue
{
public:
	BoolValue();
	BoolValue(bool value);
	BoolValue(string value);
	bool Value;
	~BoolValue();
	virtual ExpresionClassType getType();
	virtual string ToString();
};

class NumberValue :
	public ExpresionValue
{
public:
	NumberValue();
	NumberValue(int value);
	NumberValue(string value);
	int Value;
	~NumberValue();
	virtual ExpresionClassType getType();
	virtual string ToString();
};

class StringValue :
	public ExpresionValue
{
public:
	StringValue();
	StringValue(string value);
	string Value;
	~StringValue();
	virtual ExpresionClassType getType();
	virtual string ToString();
};

class CharacterValue :
	public ExpresionValue
{
public:
	CharacterValue();
	CharacterValue(char value);
	char Value;
	~CharacterValue();
	virtual ExpresionClassType getType();
	virtual string ToString();	
};