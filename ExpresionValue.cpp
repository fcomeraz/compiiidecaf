#include "ExpresionValue.h"

ArrayValue::ArrayValue()
{
}

ArrayValue::ArrayValue(list<ExpresionValue*>  value)
{
	this->Value = value;
}

string ArrayValue::ToString(){
	string ret = "este es un array";
	return ret;
}

ArrayValue::~ArrayValue()
{
}
ExpresionClassType ArrayValue::getType(){
	return ArrayValueClass;
}

BoolValue::BoolValue()
{
}
BoolValue::BoolValue(bool value)
{
	this->Value = value;
}
BoolValue::BoolValue(string value)
{
	transform(value.begin(), value.end(), value.begin(), ::tolower);
	istringstream is(value);
	bool b;
	is >> std::boolalpha >> b;
	this->Value = b;
}

string BoolValue::ToString(){
	string t = "BoolValue: ";
	t.append(this->Value?"true":"false");
	return t;
}

BoolValue::~BoolValue()
{
}
ExpresionClassType BoolValue::getType()
{
	return BoolValueClass;
}

NumberValue::NumberValue()
{
}
NumberValue::NumberValue(int value)
{
	this->Value = value;
}

NumberValue::NumberValue(string value){
	if(!strncmp(value.c_str(), "0x", 2)){
		Value = 0;
		for(int i = 2; i < value.length(); i++){
			char c = value.c_str()[i];
			int cvalue;
			if(c >= 97 && c <= 102) cvalue = c-87;
			if(c >= 65 && c <= 70) cvalue = c-55;
			if(c >= 48 && c <= 57) cvalue = c-48;
			Value+=cvalue*pow(16.0, (double)(value.length() - (i+1)));
		}
	}else{
		this->Value = atoi(value.c_str());
	}
}

string NumberValue::ToString(){
	string ret = "NumberValue: ";
	ostringstream number;
	number << this->Value;
	ret.append(number.str());
	return ret;
}

ExpresionClassType NumberValue::getType()
{
	return NumberValueClass;
}

NumberValue::~NumberValue()
{
}

StringValue::StringValue()
{
}
StringValue::StringValue(string value)
{
	this->Value = value;
}

string StringValue::ToString(){
	string ret = "StringValue: ";
	ret.append(this->Value);
	return ret;
}


StringValue::~StringValue()
{

}


ExpresionClassType StringValue::getType()
{
	return StringValueClass;
}

CharacterValue::CharacterValue(){

}

CharacterValue::CharacterValue(char value){
	this->Value = value;
}

CharacterValue::~CharacterValue(){

}

ExpresionClassType CharacterValue::getType(){
	return CharacterValueClass;
}

string CharacterValue::ToString(){
	string ret = "CharacterValue: ";
	ostringstream oss;
	oss << this->Value;
	ret.append(oss.str());
	return ret;
}

