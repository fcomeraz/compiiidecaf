TARGET = decaf

rebuild: clean all

all: ${TARGET}

${TARGET}: ExpresionNode.o StatementNode.o ExpresionValue.o BaseType.o SymbolsTable.o ${TARGET}_parser.o ${TARGET}_lexer.o
	g++ -o $@ $^

ExpresionNode.o: ExpresionNode.cpp ExpresionNode.h
	g++ -c -o $@ $<
	
StatementNode.o: StatementNode.cpp StatementNode.h
	g++ -c -o $@ $<
	
ExpresionValue.o: ExpresionValue.cpp ExpresionValue.h
	g++ -c -o $@ $<
	
BaseType.o: BaseType.cpp BaseType.h
	g++ -c -o $@ $<
	
SymbolsTable.o: SymbolsTable.cpp SymbolsTable.h
	g++ -c -o $@ $<
		
${TARGET}_parser.o: ${TARGET}_parser.cpp
	g++ -c -o $@ $<

${TARGET}_lexer.o: ${TARGET}_lexer.cpp
	g++ -c -o $@ $<

${TARGET}_parser.cpp: ${TARGET}.y
	bison -v -Werror --report=all --defines=tokens.h -o $@ $<


${TARGET}_lexer.cpp: ${TARGET}.l
	flex -o $@ $<	

clean:
	rm -rf *.o
	rm -rf ${TARGET}_parser.cpp
	rm -rf ${TARGET}_lexer.cpp
	rm -rf ${TARGET}
	rm -rf decaf_parser.output
	rm -rf tokens.h