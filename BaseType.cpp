#include "BaseType.h"


ArrayType::ArrayType()
{
}


ArrayType::~ArrayType()
{
}

bool ArrayType::IsAssignable(BaseType * type)
{
	if (typeid(*type) != typeid(ArrayType*))
	{
		return false;
	}
	ArrayType * arrayType = (ArrayType *)type;

	if (arrayType->Dimensions.size() != Dimensions.size())
		return false;

	for (VarDimensions::iterator i = Dimensions.begin(),j = arrayType->Dimensions.begin(); i != Dimensions.end(); i++,j++)
	{
		if (i != j)
			return false;
	}
	return OfType->IsAssignable(arrayType->OfType);
}

ExpresionValue * ArrayType::GetDefaultValue()
{
	//TODO: pendiente agregar el size al arreglo
	int size = 1;
	
	list<ArrayValue*> listDefault;
	
	listDefault.push_back(new ArrayValue());
	return new ArrayValue();

}

ExpresionValue * ArrayType::Parse(string inputValue)
{

	return NULL;
}

TypeClassType ArrayType::getType()
{
	return ArrayTypeClass;
}



BoolType::BoolType()
{
}
bool BoolType::IsAssignable(BaseType * type)
{
	return typeid(*type) == typeid(BoolType);
}

ExpresionValue * BoolType::GetDefaultValue()
{
	return new BoolValue(0);
}

ExpresionValue * BoolType::Parse(string inputValue)
{
	return new BoolValue(inputValue);
}

BoolType::~BoolType()
{
}
TypeClassType BoolType::getType()
{
		return BoolTypeClass;
}

NumberType::NumberType()
{
}


bool NumberType::IsAssignable(BaseType * type)
{
	return typeid(*type) == typeid(NumberType);
}

ExpresionValue * NumberType::GetDefaultValue()
{
	return new NumberValue(0);
}

ExpresionValue * NumberType::Parse(string inputValue)
{
	return new NumberValue(inputValue);
}

NumberType::~NumberType()
{
}
TypeClassType NumberType::getType()
{
	return NumberTypeClass;
}

StringType::StringType()
{
}


StringType::~StringType()
{
}

bool StringType::IsAssignable(BaseType * type)
{
	return typeid(*type) == typeid(StringType);
}

ExpresionValue * StringType::GetDefaultValue()
{
	return new StringValue("");
}

ExpresionValue * StringType::Parse(string inputValue)
{
	return new StringValue(inputValue);
}
TypeClassType StringType::getType()
{
	return StringTypeClass;
}

CharacterType::CharacterType(){

}

CharacterType::~CharacterType(){

}

bool CharacterType::IsAssignable(BaseType * type){
	return typeid(*type) == typeid(StringType);
}

ExpresionValue * CharacterType::GetDefaultValue(){
	return new CharacterValue(' ');
}

ExpresionValue * CharacterType::Parse(string inputValue){
	if(inputValue.length() == 0)
		return NULL;
	return new CharacterValue(inputValue.c_str()[0]);
}

TypeClassType CharacterType::getType(){
	return CharacterTypeClass;
}
